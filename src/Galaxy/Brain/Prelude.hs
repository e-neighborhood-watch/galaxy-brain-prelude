{-# Language QuantifiedConstraints #-}
{-# Language MultiParamTypeClasses #-}
{-# Language ScopedTypeVariables #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language TypeApplications #-}
{-# Language ConstraintKinds #-}
{-# Language KindSignatures #-}
{-# Language RankNTypes #-}
{-# Language PolyKinds #-}
module Galaxy.Brain.Prelude
  ( {- * Category -} Category (..)
  , Representational2
  , {- * Functor -} Functor (..)
  , Endofunctor
  , {- * Optics -} LensLike
  , Lens
  --, AGetter
  )
  where

import qualified Prelude as Prelude ()

import Control.Category
  ( Category
    ( (.)
    , id
    )
  )
import Data.Kind
  ( Type
  , Constraint
  )
import Data.Functor.Const
  ( Const
    ( Const
    )
  , getConst
  )
import Data.Coerce
  ( coerce
  , Coercible
  )


-- | The 'Functor' indicates a type can be mapped over. 
--
-- Instances of 'Functor' should obey these laws:
--
-- > map id = id
-- > map (f . g) = (map f) . (map g)
class
  ( Category s
  , Category t
  )
    => Functor (s :: a -> a -> Type) (t :: b -> b -> Type) (f :: a -> b)
  where
    fmap :: s x y -> t (f x) (f y)

instance Category s => Functor s (->) (s a) where
  fmap = (.)

type Representational2 (p :: k -> k -> Type) =
  (forall x x' y y'. (Coercible x x', Coercible y y') => Coercible (p x y) (p x' y')) :: Constraint

instance (Category s, Category t, Representational2 t) => Functor s t (Const a) where
  fmap _ = coerce (id :: t a a) -- or: id @t @a

--instance (Category s, Category t) => Functor s t (Const a) where
--  -- fmap :: s x y -> t (Identity x) (Identity y)
--  fmap = fmap

-- | An 'Endofunctor' is a 'Functor' from a category to itself.
type Endofunctor s f = Functor s s f

type LensLike f cat1 cat2 s t a b =
  ( Category cat1
  , Category cat2
  )
    => cat1 a (f b) -> cat2 s (f t)

type Lens cat1 cat2 s t a b =
  forall f . Functor cat1 cat2 f => LensLike f cat1 cat2 s t a b

type Getting cat1 cat2 r s a =
  Representational2 cat2 => LensLike (Const r) cat1 cat2 s s a a

{-
-- TODO use MonadReader for more abstraction
view ::
  Getting cat1 cat2 a s a -> (s -> a)
view l = getConst 
-}
