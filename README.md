# galaxy-brain-prelude

Galaxy Brain Prelude is an "improvement" for Haskell's `Prelude` library.
The goal is to create a library which implements all of the same functionality as `Prelude` but
using advanced Haskell techniques even (or especially) where they are not needed or useful.

The final product should ...

 * Use only type signatures that are very long and generally uninformative.
 * Contain no function that has a more general version.
 * Use types to prevent bad programs to the fullest extent.

 ## Cheatsheet

 This cheetsheet should keep a running tally of all the prelude functions and type classes and their ultimate fates.  As functions get improved or replaced they will be added to this list.

| Prelude object                                | Fate                                       |
| --------------------------------------------- | -----------------------------------------  |
| `id :: a -> a`                                | Replaced by the more general `ask`.        |
| `const :: a -> b -> a`                        | Replaced by the more general `pure`.       |
| `fmap :: Functor f => (a -> b) -> f a -> f b` | Replaced by `over mapped`.                 |
| `($>) :: Functor f => b -> f a -> f b`        | Replaced by `set mapped`.                  |
